<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UploadingAndDisplayingSongsController;
use App\Http\Controllers\CreatingAndDisplayingPlaylists;
use App\Http\Controllers\AddingAndListingSongs;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('songs',[UploadingAndDisplayingSongsController::class,'getData']);
Route::post('songs',[UploadingAndDisplayingSongsController::class,'getData']);
Route::view('playlists',[CreatingAndDisplayingPlaylists::class,'getPlaylist']);
Route::post('playlists',[CreatingAndDisplayingPlaylists::class,'getPlaylist']);
Route::view('playlist_songs',[AddingAndListingSongs::class,'getPlaylistSong']);
Route::post('playlist_songs',[AddingAndListingSongs::class,'getPlaylistSong']);


